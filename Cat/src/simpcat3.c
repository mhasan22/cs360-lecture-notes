/* simpcat3.c
   James S. Plank
   CS360
   September, 1996 */

/* Using fread()/fwrite(). */

#include <stdio.h>

int main()
{
  char c;
  int i;

  while (fread(&c, 1, 1, stdin) == 1) {
    fwrite(&c, 1, 1, stdout);
  }
  return 0;
}
