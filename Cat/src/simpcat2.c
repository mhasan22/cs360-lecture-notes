/* simpcat2.c
   James S. Plank
   CS360
   September, 1996 */

/* Using read()/write(). */

#include <unistd.h> 
                       
int main()                 
{                      
  char c;           
                       
  while(read(0, &c, 1) == 1) {
    write(1, &c, 1);    
  }                    
  return 0;
}                      

