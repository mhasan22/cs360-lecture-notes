#include <stdio.h>
#include <stdlib.h>

void a(unsigned long *d, unsigned int *j, unsigned int *k)
{
  int i;
  printf("d    = 0x%016lx\n", (unsigned long) d);
  printf("j    = 0x%016lx\n", (unsigned long) j);
  printf("k    = 0x%016lx\n\n", (unsigned long) k);
  printf("*j   = 0x%08x\n", *j);
  printf("*k   = 0x%08x\n\n", *k);
  
  for (i = 0; i < 10; i++) printf("d[%d] = 0x%016lx\n", i, d[i]);
}

int main()
{
  unsigned long *d;
  unsigned int *j, *k;
  unsigned long id;
  int i;

  srand48(995);
  d = (unsigned long *) malloc(sizeof(long)*12);
  k = (unsigned int *) d;
  d++;
  id = (unsigned long) k;
  j = (unsigned int *) (d+10);
  *j = 0xe3c017ff;
  *k = 0xc01705;

  for (i = 0; i < 10; i++) d[i] = id + ((int) (drand48() * 12)) * sizeof(long);
  a(d, j, k);
  return 0;
}
