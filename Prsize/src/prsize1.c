/* prsize1.c
   James S. Plank
   Fall, 1996
   CS360 
   Latest tweaks to keep it up to date: February, 2021. */
  
/* This program prints the size of all files in the current directory. */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>

int main()
{
  DIR *d;                   /* Return value of opendir(). */
  struct dirent *de;        /* Return value of each readdir() call. */
  struct stat buf;          /* The information about each file returned by stat() */
  int exists;               /* Return value of stat on each file. */
  long total_size;          /* The total size of all files. */

  d = opendir(".");         /* Open "." to list all the files. */
  if (d == NULL) {
    perror(".");
    exit(1);
  }
 
  total_size = 0;

  /* Run through the directory and run stat() on each file, 
     keeping track of the total size of all of the files.  */

  for (de = readdir(d); de != NULL; de = readdir(d)) {
    exists = stat(de->d_name, &buf);
    if (exists < 0) {
      fprintf(stderr, "Couldn't stat %s\n", de->d_name);
    } else {
      total_size += buf.st_size;
    }
  }

  /* Although the closedir call isn't necessary, it will be later... */

  closedir(d);
  printf("%ld\n", total_size);
  return 0;
}
