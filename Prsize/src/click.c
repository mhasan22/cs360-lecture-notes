#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char buf[100];

void printmemory(char *address, int nn)
{
  int i;
  long *b;

  b = (long *) address;
  for (i = 0; i < nn; i++) {
    printf("0x%lx 0x%lx\n", (unsigned long) (b+i), b[i]);
  }
}

typedef unsigned int UI;

void pm(unsigned long *p)
{
  unsigned long **q;
  unsigned long ***r;
  unsigned long  ****s;

  q = (unsigned long **) p;
  r = (unsigned long ***) p;
  s = (unsigned long ****) p;

  printf("1: 0x%02lx\n", (*p) & 0xff);
  printf("2: 0x%02lx\n", (**q) & 0xff);
  printf("3: 0x%02lx\n", (***r) & 0xff);
  printf("4: 0x%02lx\n", (****s) & 0xff);

  printf("5: 0x%02lx\n", (p[1]) & 0xff);
  printf("6: 0x%02lx\n", (q[1][1]) & 0xff);
  printf("7: 0x%02lx\n", (r[1][1][1]) & 0xff);
  printf("8: 0x%02lx\n", (s[1][1][1][1]) & 0xff);
}

int main()
{
  unsigned long *buf;
  int bint;
  int i, tmp, j;

  buf = (unsigned long *) malloc(sizeof(long)*20);
  srand48(45);

  for (i = 0; i < 20; i++) {
    buf[i] = (unsigned long) (buf + (int) (20 * drand48()));
  }

  printmemory((char *) buf, 20);
  pm(buf);
  return 0;
}
