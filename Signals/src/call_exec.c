/* Test to see if alarm() works even after an exec call. */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
  alarm(2);
  execlp("cat", "cat", NULL);
  perror("execl");
  return 0;
}
