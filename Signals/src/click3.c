#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
  FILE *f;

  f = fopen("f3.txt", "w");
  fprintf(f, "Fred\n");
  alarm(1);
  fork();
  sleep(2);
  fprintf(f, "Binky\n");
  fclose(f);
  return 0;
}
