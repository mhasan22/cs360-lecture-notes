#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
  FILE *f;

  f = fopen("f2.txt", "w");
  fprintf(f, "Fred\n");
  fork();
  fork();
  fprintf(f, "Binky\n");
  fclose(f);
  return 0;
}
