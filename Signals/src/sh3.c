/* Use alarm() / SIGALRM to interrupt the program every second and print out its state. */

#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int i, j, seconds;         // Global variables, because we're accessing them in the signal handler.

/* Print out the time and the state, and then generate SIGALRM in another second. */
void alarm_handler(int dummy)
{
  seconds++;
  printf("%d second%s just passed: j = %4d.  i = %6d\n", seconds,
     (seconds == 1) ? " " : "s", j, i);
  signal(SIGALRM, alarm_handler);
  alarm(1);
}

int main()
{
  seconds = 0;

  /* Register the signal handler, and generate SIGALRM in a second. */

  signal(SIGALRM, alarm_handler);
  alarm(1);

  /* Spin the CPU */

  for (j = 0; j < 5000; j++) {
    for (i = 0; i < 1000000; i++);
  }
  return 0;
}
