#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
  int fd;
  int child, parent, status;
  char c;

  fd = open("f1.txt", O_RDONLY);

  child = (fork() == 0);
  parent = !child;

  if (child) sleep(1);
  read(fd, &c, 1);
  if (parent) wait(&status);
  printf("%c\n", c);

  return 0;
}
