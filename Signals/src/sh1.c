/* Write a signal handler for CNTL-C */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

/* This is the signal handler that we are setting up for Control-C.  
   Instead of doing the default action, it will call this handler. */

void cntl_c_handler(int dummy)
{
  signal(SIGINT, cntl_c_handler);
  printf("You just typed cntl-c\n");
}

int main()
{
  int i, j;

  /* Register the signal handler with the operating system. */

  signal(SIGINT, cntl_c_handler);

  /* This code spins the CPU for a little while before exiting. */

  for (j = 0; j < 5000; j++) {   
    for (i = 0; i < 1000000; i++);
  }
  return 0;
}
