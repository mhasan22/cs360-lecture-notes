<title>CS360 Lecture notes -- Signals</title>
<body bgcolor=ffffff>
<h1>CS360 Lecture notes -- Signals</h1>
<UL>
<LI><a href=http://web.eecs.utk.edu/~jplank>James S. Plank</a>
<LI>Directory: <b>/home/plank/cs360/notes/Signals</b>
<LI>Lecture notes:
    <a href=http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Signals/lecture.html>
    <b>
 http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Signals/lecture.html
</b></a>
<LI> Most recently revised:<i>
Mon Mar 29 14:44:46 EDT 2021
</i>
</UL>
<hr>

<h1>Signals</h1>

<p>Signals are complex flow-of-control operations.  A signal is an
interruption of the program of some sort.  For example, when you press
<b>CNTL-C</b>, that sends the <b>SIGINT</b> signal to your program.
When you press <b>CNTL-\</b>, that sends the <b>SIGQUIT</b> signal to
your program.  When you generate a segmentation violation, that sends
the <b>SIGSEGV</b> signal to your program.  

<p>Your program has various ways of dealing with
signals.  By default, there are certain actions
that take place.  For example, which you press
<b>CNTL-C</b> or <b>CNTL-\</b>, the program usually exits.  
That is the default action for <b>SIGINT</b> and <b>SIGQUIT</b>.  
When you get a segmentation violation, your program
spits out an error message, potentially creates a core dump, and
then exits.  That is the default action for
<b>SIGSEGV</b>. 

<p>You can redefine what happens when you get these
signals, which allows you to write very flexible
programs.  Internally, when a signal is
generated, the operating system takes over from
the currently running program.  It saves the
current state of the program on the stack.  Then,
it calls an "interrupt handler" for the specific
signal.  For example, the default interrupt
handlers for <b>SIGINT</b> and <b>SIGQUIT</b> cause the program to exit.
The default interrupt handler for <b>SIGSEGV</b>
prints and error, potentially causes the program to dump core, and then
exits.  If the interrupt handler for a signal calls
<b>return</b>, then what happens is that the operating
system takes over again, and restores the program
from the state that it has saved on the stack.
The program resumes from where it left off.

<p>You can use the <b>signal()</b> function to define
interrupt handlers for signals.  As always, read
the man page: <b>man signal</b>.

<p>For example, look at 
<b><a href=src/sh1.c>src/sh1.c</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Write a signal handler for CNTL-C */</font>

#include &lt;signal.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

<font color=blue>/* This is the signal handler that we are setting up for Control-C.  
   Instead of doing the default action, it will call this handler. */</font>

void cntl_c_handler(int dummy)
{
  signal(SIGINT, cntl_c_handler);
  printf("You just typed cntl-c\n");
}

int main()
{
  int i, j;

  <font color=blue>/* Register the signal handler with the operating system. */</font>

  signal(SIGINT, cntl_c_handler);

  <font color=blue>/* This code spins the CPU for a little while before exiting. */</font>

  for (j = 0; j &lt; 5000; j++) {   
    for (i = 0; i &lt; 1000000; i++);
  }
  return 0;
}
</pre></td></table></center><p>

<p>What this does is set up a signal handler for
<b>SIGINT</b>.  Now, when the user presses <b>CNTL-C</b>, the
operating system will save the current execution
state of the program, and then execute
<b>cntl_c_handler</b>.  When <b>cntl_c_handler</b> returns, the
operating system resumes the program from where
it was interrupted.  Thus, when you run <b>sh1</b>, each
time you type <b>CNTL-C</b>, it will print <b>"You just typed cntl-c"</b>, 
and the program will continue.  It
will exit by itself in 10 seconds or so. 

<p>The signal handler should follow the prototype of <b>cntl_c_handler</b>.
In other words it should return a <b>(void)</b> (i.e. nothing), and should
accept an integer argument, even if it will not use the argument.
Otherwise, <b>gcc</b> will complain to you.

<p>Also, note that I make a <b>signal()</b> call in the signal handler.
On some systems, if you do <i>not</i> do this, then it will reinstall
the default signal handler for <b>CNTL-C</b> once it has handled the
signal.  On some systems, you don't have to make the extra <b>signal()</b>
call.  Such is life in the land of multiple Unix's.

<hr>
<p>You can handle each different signal with a call
to <b>signal()</b>.  For example, 
<a href=src/sh1a.c><b>src/sh1a.c</b></a>
defines different
signal handlers for <b>CNTL-C</b> (which is <b>SIGINT</b>),
and <b>CNTL-\</b> (which is <b>SIGQUIT</b>).  They print out
the values of <b>i</b> and <b>j</b> when the signal is
generated.  Note that <b>i</b> and <b>j</b> must be global
variables for this to work.  This is one example
when you have to use global variables.

<p>Try this out by compiling the program and then running it, and pressing
<b>CNTL-C</b> and <b>CNTL-\</b> a bunch of times:

<pre>
UNIX> <font color=darkred><b>bin/sh1a</b></font>
<font color=darkred><b>^C</b></font>You just typed cntl-c.  j is 1748 and i is 399828
<font color=darkred><b>^\</b></font>You just typed cntl-\.  j is 1859 and i is 99015
<font color=darkred><b>^C</b></font>You just typed cntl-c.  j is 1980 and i is 198717
<font color=darkred><b>^\</b></font>You just typed cntl-\.  j is 2142 and i is 304705
<font color=darkred><b>^C</b></font>You just typed cntl-c.  j is 2244 and i is 220206
<font color=darkred><b>^\</b></font>You just typed cntl-\.  j is 2402 and i is 259873
<font color=darkred><b>^C</b></font>You just typed cntl-c.  j is 2535 and i is 785388
<font color=darkred><b>^\</b></font>You just typed cntl-\.  j is 2693 and i is 3998
<font color=darkred><b>^C</b></font>You just typed cntl-c.  j is 2852 and i is 14044
<font color=darkred><b>^C</b></font>You just typed cntl-c.  j is 4921 and i is 763113
UNIX>
</pre>

<p>You can also catch the segmentation violation signal.  One 
of those CS legends is that some grad student used to 
put the following into his code:

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;signal.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

void segv_handler(int dummy)
{
  signal(SIGINT, sint_handler);
  fprintf(stderr, "nfs server not responding, still trying\n");
  while(1) ;
}

int main()
...
  signal(SIGSEGV, segv_handler();

  rest of the code
}
</pre></td></table></center><p>

This is so that if he was demo-ing his code, and a segmentation violation
occured (which always seems to happen when you're demo-ing code), it 
would look like the network had frozen.  Very clever.  (I.e. look at
and run 
<a href=src/sh1b.c><b>src/sh1b.c</b></a>.
It should cause a segmentation violation, but instead
looks like the network is hanging).  Note also that <b>CNTL-C</b> is disabled, since that's
what happens when NFS hangs.

<hr>
<h1>Alarm()</h1>

<p>Another use of signal handlers is the "alarm
clock" that Unix provides.  Read the man page for
<b>alarm()</b>.  What <b>alarm(n)</b> does is
return, and then <b>n</b> seconds later, it will cause
the <b>SIGALRM</b> signal to occur.  If you have set a
signal handler for it, then you can catch the
signal, and do whatever it is that you wanted to
do.  For example, 
<a href=sh2.c><b>sh2.c</b></a>
is like 
<a href=sh1.c><b>sh1.c</b></a>, only it
prints out a message after the program has
executed 3 seconds.  <b>alarm()</b> is
approximate -- it's not exactly 3 seconds, but
we'll consider it close enough for the purposes
of this class.  

<pre>
UNIX> <font color=darkred><b>bin/sh2</b></font>
Three seconds just passed: j = 1245.  i = 287469
UNIX>
</pre>

Finally, 
<a href=src/sh3.c><b>src/sh3.c</b></a>
shows how you can get Unix to send you <b>SIGALRM</b> every second.
It's just a tweak to 
<a href=src/sh2.c><b>src/sh2.c</b></a>
where you have the alarm handler call <b>alarm()</b>
to make Unix generate <b>SIGALRM</b> one second after the current one.  
I'll include the code below:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Use alarm() / SIGALRM to interrupt the program every second and print out its state. */</font>

#include &lt;signal.h&gt;
#include &lt;unistd.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int i, j, seconds;         <font color=blue>// Global variables, because we're accessing them in the signal handler.</font>

<font color=blue>/* Print out the time and the state, and then generate SIGALRM in another second. */</font>

void alarm_handler(int dummy)
{
  seconds++;
  printf("%d second%s just passed: j = %4d.  i = %6d\n", seconds,
     (seconds == 1) ? " " : "s", j, i);
  signal(SIGALRM, alarm_handler);
  alarm(1);
}

int main()
{
  seconds = 0;

  <font color=blue>/* Register the signal handler, and generate SIGALRM in a second. */</font>

  signal(SIGALRM, alarm_handler);
  alarm(1);

  <font color=blue>/* Spin the CPU */</font>

  for (j = 0; j &lt; 5000; j++) {
    for (i = 0; i &lt; 1000000; i++);
  }
  return 0;
}
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>bin/sh3</b></font>
1 second  just passed: j =  805.  i = 305534
2 seconds just passed: j = 1608.  i = 783876
3 seconds just passed: j = 2419.  i =  77897
4 seconds just passed: j = 3217.  i = 643283
5 seconds just passed: j = 4019.  i =  30497
6 seconds just passed: j = 4823.  i = 907698
UNIX> <font color=darkred><b></b></font>
</pre>


On some systems, when you are in a signal handler for one signal, 
you cannot process that same signal again until the handler returns.  
On other systems, you can handle that same signal again.
For example, look at 
<a href=sh4.c><b>sh4.c</b></a> -- here's the code for the signal
handlers:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Put a while loop into the alarm handler, to see if it
   can catch SIGALRM while it's in the alarm handler. */</font>

int i, j;

void cntl_bs_handler(int dummy)
{
  printf("SIGQUIT generated: j = %d.  i = %d\n", j, i);
  signal(SIGQUIT, cntl_bs_handler);
}

void alarm_handler(int dummy)
{
  printf("One second has passed: j = %d.  i = %d\n", j, i);
  signal(SIGALRM, alarm_handler);
  alarm(1);
  while(1);
}
</pre></td></table></center><p>

<p><b>Alarm_handler()</b> has an infinite loop in it, meaning that
it never returns.   The program runs for a second, and then <b>SIGALRM</b>
is generated, and <b>alarm_handler()</b> is entered.  It goes into
an infinite loop, and one second later, <b>SIGALRM</b> is generated
again.  Depending on your version of Unix, different things may happen.
For example, on my Macbook in 2021, I got the following:

<pre>
UNIX> <font color=darkred><b>bin/sh4</b></font>
One second has passed: j = 476.  i = 853593
<font color=blue>(and it hangs)</font>
</pre>

In other words, the signal was to be ignored until
you return from <b>alarm_handler()</b>, which of course never happened.

<p>Here was the output on Solaris in something like 1996:

<pre>
UNIX> <font color=darkred><font color=darkred><b>sh4</b></font></font>
One second has passed: j = 7.  i = 697646
One second has passed: j = 7.  i = 697646
One second has passed: j = 7.  i = 697646
One second has passed: j = 7.  i = 697646
...
</pre>

In other words, you could catch <b>SIGALRM</b> from <b>SIGALRM</b>.

Which output does your operating system produce?
<p>
You can generate and handle other signals reliably whether in a signal
handler or not.  For example, when
you press <b>CNTL-\</b> in <b>sh4.c</b>, it gets caught properly 
whether the program 
or the <b>alarm_handler()</b> is running -- give it a try.

Here's an interesting question -- when <b>SIGALRM</b> is generated, and I'm 
in the alarm handler code, is the signal simply ignored, or is it queued until
the alarm handler returns?  Here's a program,
(<b><a href=src/sh5.c>src/sh5.c</a></b>),
that tests this:  

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Test to see if SIGALRM is queued up if it's already in the alarm handler. */</font>

#include &lt;signal.h&gt;
#include &lt;unistd.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int i, j;

<font color=blue>/* Generate SIGALRM while in the alarm handler, but eventually return. */</font>

void alarm_handler(int dummy)
{
  printf("One second has passed: j = %d.  i = %d\n", j, i);
  signal(SIGALRM, alarm_handler);
  alarm(1);

  for (; j &lt; 5000; j++) {
    for (i = 0; i &lt; 1000000; i++);
  }
}

int main()
{
  signal(SIGALRM, alarm_handler);
  alarm(1);

  for (j = 0; j &lt; 5000; j++) {
    for (i = 0; i &lt; 1000000; i++);
  }
  return 0;
}
</pre></td></table></center><p>
When you run this program, here's the output: 

<pre>
UNIX> <font color=darkred><b>bin/sh5</b></font>
One second has passed: j = 817.  i = 645456
One second has passed: j = 5000.  i = 1000000
UNIX> <font color=darkred><b></b></font>
</pre>

Here's what happened:

<UL>
<LI> The <b>main()</b> runs until <b>j</b>=817 and <b>i=645456</b>.
<LI> <b>SIGALRM</b> is generated, and the alarm handler runs, calling <b>alarm(1)</b> and 
     then continuing to update <b>j</b> and <b>i</b>.  
<LI> One second later, <b>SIGALRM</b> is generated again, but since the alarm handler is
     running, it is queued in the operating system.
<LI> When the first alarm handler returns, <b>SIGALRM</b> is delivered to the program, and
    the alarm handler is called for a second time.  It prints that <b>j</b> is 5000, and 
    <b>i</b> is 1000000, and returns; it calls <b>alarm(1)</b> again, and then returns.  
<LI> <b>Main()</b> then returns, and the program returns.  
</UL>

<p>Finally, you can send any signal to a program
with the <b>kill</b> command.  Read the man page.
Signal number 9 (<b>SIGKILL</b>) cannot be caught by
your program, meaning that you cannot write a
signal handler for it.  This is nice because if
you mess up writing a signal handler, then "<b>kill -9</b>" 
is the only way to kill the program.  

<hr>
<h3>Alarm() and exec</h3>

Here's another interesting question: If you call <b>alarm(2)</b>, and then 
you call <b>execlp</b> or one of its variants, then your memory is overwritten
by a new program, and the <b>execlp()</b> call doesn't return (because it can't).
Does the <b>SIGALRM</b> signal still get delivered?

<p>
Here is a simple program 
(<b><a href=src/call_exec.c>src/call_exec.c</a></b>)
to answer that question:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Test to see if alarm() works even after an exec call. */</font>

#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;unistd.h&gt;

int main()
{
  alarm(2);
  execlp("cat", "cat", NULL);
  perror("execl");
  return 0;
}
</pre></td></table></center><p>
It calls <b>alarm(2)</b>, and then <b>execlp</b> for the program <b>cat</b>.  That program
will block, reading from standard input.  After two seconds, it dies, because <b>SIGALRM</b>
was delivered to it!

<pre>
UNIX> <font color=darkred><b>bin/call_exec</b></font>
<font color=blue>(Two seconds pass)</font>
Alarm clock: 14
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>SIGPIPE and the Stdio Library</h3>

When you write to a file descriptor that has no read end, the <b>SIGPIPE</b> signal will
be generated.  If uncaught, it will kill your program.  Unfortunately, since the operating
system often does its own buffering of writes, you don't usually generate <b>SIGPIPE</b>
as soon as you should.  It usually comes later.  This can prove difficult to handle.
<p>
Let's take a look at the interaction of <b>SIGPIPE</b> and the standard I/O library, since
that's usually where you are when <b>SIGPIPE</b> is generated.  Take a look at
<b><a href=src/sigpipe_1.c>src/sigpipe_1.c</a></b>

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program repeatedly writes the string ABCDEFGHIJKLMNOPQRSTUVWXYZ to stdout, 
   testing the return values of fputs() and fflush() to see if there's
   a problem. */</font>

#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int main()
{
  int i;
  char *s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n";

  i = 0;
  while (1) {
    if (fputs(s, stdout) == EOF) {
      fprintf(stderr, "Died on iteration %d on fputs\n", i);
      exit(0);
    }
    if (fflush(stdout) != 0) {
      fprintf(stderr, "Died on iteration %d on fflush\n", i);
      exit(0);
    }
    fprintf(stderr, "Iteration %d done\n", i);
    i++;
  }
  return 0;
}
</pre></td></table></center><p>

We're going to pipe this program into "<b>head -n 1</b>."  You would think that
this would generate <b>SIGPIPE</b> on the second <b>fflush()</b>, since <b>head</b>
will exit after reading the first line.  However, we get something different from
what we expect (the exact output will of course differ from machine to machine and
even from run to run):

<pre>
UNIX> <font color=darkred><b>bin/sigpipe_1 | head -n 1</b></font>
Iteration 0 done
Iteration 1 done
...
Iteration 2414 done
ABCDEFGHIJKLMNOPQRSTUVWXYZ
Iteration 2415 done
Iteration 2416 done
UNIX> <font color=darkred><b>bin/sigpipe_1 | head -n 1</b></font>
Iteration 0 done
Iteration 1 done
...
Iteration 2094 done
ABCDEFGHIJKLMNOPQRSTUVWXYZ
Iteration 2095 done
Iteration 2096 done
UNIX> 
</pre>

Since we don't get an error statement, we can assume that <b>SIGPIPE</b> was generated
and killed the process.  
<b><a href=src/sigpipe_2.c>src/sigpipe_2.c</a></b> catches the signal and ignores it:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This is the same as sigpipe_1.c, except we catch and ignore SIGPIPE. */</font>

#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;signal.h&gt;

void sigpipe_handler(int dummy)
{
  fprintf(stderr, "Ignoring Sigpipe()\n");
  signal(SIGPIPE, sigpipe_handler);
}

<font color=blue>(The rest is the same as sigpipe_1.c)</font>
</pre></td></table></center><p>

Now, we see that <b>SIGPIPE</b> is indeed generated.  Better yet, when we 
ignore it, the subsequent <b>fflush()</b> fails:

<pre>
UNIX> <font color=darkred><b>bin/sigpipe_2 | head -n 1</b></font>
Iteration 0 done
Iteration 1 done
...
Iteration 2469 done
ABCDEFGHIJKLMNOPQRSTUVWXYZ
Ignoring Sigpipe()
Died on iteration 2470 on fflush
UNIX> 
</pre>

(When you run this, you may find that it fails on <b>fputs()</b> instead of <b>fflush()</b>).
<p>
With this information, you should take the following statement as
gospel in your Lab 9:

<UL>
<I>When the read end of a pipe or socket goes away, <b>fputs()</b> and <b>fflush()</b> will
not generate <b>SIGPIPE</b> immediately.  The bytes are buffered in the operating system and
will be thrown away eventually.  However, the standard I/O calls will return without error
for a while.  Eventually, they will return with errors and they may or may not 
generate <b>SIGPIPE</b>.  Your best bet is to catch and ignore <b>SIGPIPE</b> and always
test return values of the stdio calls to determine when the read end of the pipe has gone away.
</i>
</UL>

Since <b>head</b> is most likely written with the stdio library, it performs buffering too,
so perhaps it's not surprising that <b>SIGPIPE</b> is not generated for many iterations.
However, were the operating system not buffering, the output of <b>bin/sigpipe_2 | head -n 1</b>
would be deterministic (it would always be the same).
<p>
To hammer this home further, check out
<b><a href=src/instaclose.c>src/instaclose.c</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Close standard input and then wait a second, before exiting. */</font>

#include &lt;stdio.h&gt;
#include &lt;unistd.h&gt;
#include &lt;stdlib.h&gt;

int main()
{
  close(0);
  sleep(1);
  exit(0);
}
</pre></td></table></center><p>

This does no reading from standard input before closing it.  When we call it, we still get 
nondeterministic output:

<pre>
UNIX> <font color=darkred><b>bin/sigpipe_2 | bin/instaclose</b></font>
Iteration 0 done
...
Iteration 46 done
Ignoring Sigpipe()
Died on iteration 47 on fflush
UNIX> <font color=darkred><b>bin/sigpipe_2 | bin/instaclose</b></font>
Ignoring Sigpipe()
Died on iteration 0 on fflush
UNIX> <font color=darkred><b></b></font>
</pre>

Obviously, the operating system does buffering.

<hr>
<h3>Of Compilers, Optimization, and the "volatile" keyword</h3>

When optimization is enabled in the compiler, it performs quite a bit 
of analysis, turning the program into one that produces equivalent
output, but runs faster.  Sometimes, you have no clue what the optimizer
does, but you should be prepared to be mystified.  Let's go back to
<b><a href=src/sh3.c>src/sh3.c</a></b>
as an example.  When we run it without optimization, the alarm handler
is executed every second, printing out the value of <b>i</b> and <b>j</b>.

<pre>
UNIX> <font color=darkred><b>bin/sh3</b></font>
1 second  just passed: j =  812.  i = 988927
2 seconds just passed: j = 1629.  i = 866986
3 seconds just passed: j = 2461.  i = 757553
4 seconds just passed: j = 3277.  i = 335089
5 seconds just passed: j = 4101.  i = 606281
6 seconds just passed: j = 4911.  i = 922094
UNIX> <font color=darkred><b></b></font>
</pre>

Let's compile it with optimization and run it, to see what happens:

<pre>
UNIX> <font color=darkred><b>gcc -O3 src/sh3.c</b></font>
UNIX> <font color=darkred><b>./a.out</b></font>
UNIX> <font color=darkred><b>time ./a.out</b></font>

real	0m0.004s
user	0m0.001s
sys	0m0.002s
UNIX> <font color=darkred><b></b></font>
</pre>

Man, that was fast -- it seems too fast.  Back of the hand: we should be 
iterating 5,000,000,000 times.  At roughtly 2 Ghz, that should be taking
a few seconds, not 0.01 seconds.  Something's amiss.  
<p>
What's going on is that the compiler's analysis has figured out that the
loops don't do anything, so it has deleted them.
<p>
Let's make the compiler keep the loops.  How about putting this after the loop:

<p><center><table border=3 cellpadding=3><td><pre>
  printf("%d %d\n", j, i);
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>gcc -O3 src/sh3a.c</b></font>
UNIX> <font color=darkred><b>time ./a.out</b></font>
5000 1000000

real	0m0.314s
user	0m0.001s
sys	0m0.002s
UNIX> <font color=darkred><b></b></font>
</pre>

Dang it!  That's a smart compiler.  Ok -- one more try in 
<b><a href=src/sh3b.c>src/sh3b.c</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* We're trying to outsmart the compiler.  Instead of having
   our loop do nothing, we'll have it repeatedly do a small calculation.
   We're hoping that the compiler can't figure this out and delete the loop! */</font>

<font color=blue>// Everything is the same except this loop:</font>

  k = 0;
  for (j = 0; j &lt; 5000; j++) {
    for (i = 0; i &lt; 1000000; i++) k = (k &lt; 0) ? (i -j) : (j - i);
  }
  printf("%d %d %d\n", j, i, k);
  return 0;
}
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>gcc -O3 src/sh3b.c</b></font>
UNIX> <font color=darkred><b>time ./a.out</b></font>
1 second  just passed: j =    0.  i =      0
2 seconds just passed: j =    0.  i =      0
5000 1000000 995000

real	0m2.453s
user	0m2.087s
sys	0m0.003s
UNIX> <font color=darkred><b></b></font>
</pre>


There -- the compiler's not smarter than I am!!!!  But now we have another 
problem -- <b>i</b> and <b>j</b> are zero.  That can't be right.
I won't make you look through the assembler, but what has happened is that
the compiler has cached <b>i</b> and <b>j</b> into registers during <b>main()</b>,
and thus their in-memory values remain 0 and 1 while <b>main()</b> is running.
<p>
The solution to this is rather inelegant.  You need to preface the declarations
of <b>i</b> and <b>j</b> with the keyword <b>volatile</b>.  That's not a natural
keyword to me, but that tells the compiler that it needs to store <b>i</b> and <b>j</b>
in memory because the program's control flow may be weird.  That's done in 
<b><a href=sh3c.c>sh3c.c</a></b>:

<pre>
UNIX> <font color=darkred><b>gcc -O3 src/sh3c.c</b></font>
UNIX> <font color=darkred><b>time ./a.out</b></font>
1 second  just passed: j =  510.  i = 627530
2 seconds just passed: j = 1038.  i = 575758
3 seconds just passed: j = 1561.  i = 974312
4 seconds just passed: j = 2093.  i = 806465
5 seconds just passed: j = 2622.  i = 683371
6 seconds just passed: j = 3135.  i = 362346
7 seconds just passed: j = 3662.  i = 769581
8 seconds just passed: j = 4187.  i = 169098
9 seconds just passed: j = 4716.  i = 462009
5000 1000000 995000

real	0m9.889s
user	0m9.558s
sys	0m0.006s
UNIX> <font color=darkred><b></b></font>
</pre>


You'll note that the program takes 4 times longer to run, since <b>i</b> and <b>j</b>
can't be cached in registers anymore.  
<p>
Remember <b>volatile</b> when you learn <b>setjmp/longjmp</b> -- the same types of
problems can occur.

<hr>
<h3>sort_compare.c - Alternating selection and insertion sort</h3>

I typically don't get to this in class, but it's here in case you're interested.  The
program <b><a href=src/sort_compare.c>src/sort_compare.c</a></b> generates two identical
random arrays, and sorts one of them with selection sort, and the other with insertion
sort.  At each main loop, it checks a variable called <b>switch_algorithms</b>, which,
if set, changes the algorithm from insertion to selection, and back again.

<p>
And of course, we use <b>alarm()</b> and an alarm handler to set <b>switch_algorithms</b>
to one every second.  This is what causes the program to change algorithms every second:

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;stdio.h&gt;
#include &lt;signal.h&gt;
#include &lt;time.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;unistd.h&gt;

int switch_algorithms;        <font color=blue>/* This will be set to one by the alarm handler. */</font>
int selection_index;          <font color=blue>/* The index of selection sort. */</font>
int insertion_index;          <font color=blue>/* The index of insertion sort. */</font>

<font color=blue>/* The alarm handler sets switch_algorithms to one, prints out the two 
   indices, and then sets up SIGLARM to be generated again in another second. */</font>

void alarm_handler(int dummy)
{
  switch_algorithms = 1;
  printf("Selection_index: %10d        - Insertion_index: %10d\n", selection_index, insertion_index);
  signal(SIGALRM, alarm_handler);
  alarm(1);
}

<font color=blue>/* This generates two identical integer arrays, sorts one by
   selection sort and the other by insertion sort.  */</font>

int main(int argc, char **argv)
{
  int *array1, *array2;
  int size;
  int i, j, tmp, best;

  if (argc != 2) {
    fprintf(stderr, "usage: sort_compare size\n");
    exit(1);
  }
  size = atoi(argv[1]);
  array1 = (int *) malloc(sizeof(int) * size);
  array2 = (int *) malloc(sizeof(int) * size);
    
  for (j = 0; j &lt; size; j++) {
    array1[j] = rand();
    array2[j] = array1[j];
  }

  <font color=blue>/* Set the two indices, and set it so that SIGLARM 
     is generated in a second.  */</font>

  selection_index = 0;
  insertion_index = 0;

  signal(SIGALRM, alarm_handler);
  alarm(1);

  <font color=blue>/* While one of the sorts is not complete: */</font>

  while (selection_index &lt; size || insertion_index &lt; size) {

    <font color=blue>/* First do selection sort on array1, checking the switch_algorithms
       variables at each interation of the main loop. */</font>

    switch_algorithms = 0;
    while (!switch_algorithms && selection_index &lt; size) {
      best = selection_index;
      for (j = selection_index+1; j &lt; size; j++) {
        if (array1[j] &lt; array1[best]) best = j;
      }
      tmp = array1[best];
      array1[best] = array1[selection_index];
      array1[selection_index] = tmp;
      selection_index++;
    }

    <font color=blue>/* Second, do insertion sort on array2, checking the switch_algorithms
       variables at each interation of the main loop. */</font>

    switch_algorithms = 0;
    while (!switch_algorithms && insertion_index &lt; size) {
      tmp = array2[insertion_index];
      for (j = insertion_index-1; j &gt;= 0 && array2[j] &gt; tmp; j--) array2[j+1] = array2[j];
      j++;
      array2[j] = tmp;
      insertion_index++;
    }
  }

  printf("Done\n");
  <font color=blue>// for (i = 0; i &lt; size; i++) printf("%10d %10d\n", array1[i], array2[i]);</font>
  return 0;
}
</pre></td></table></center><p>

Here it is running -- as anticipated, insertion sort is faster than selection sort!

<pre>
UNIX> <font color=darkred><b>bin/sort_compare 100000</b></font>
Selection_index:       6998        - Insertion_index:          0
Selection_index:       6999        - Insertion_index:      46498
Selection_index:      14607        - Insertion_index:      46499
Selection_index:      14608        - Insertion_index:      65655
Selection_index:      23049        - Insertion_index:      65656
Selection_index:      23050        - Insertion_index:      80043
Selection_index:      32463        - Insertion_index:      80044
Selection_index:      32464        - Insertion_index:      92482
Selection_index:      43280        - Insertion_index:      92483
Selection_index:      47380        - Insertion_index:     100000
Selection_index:      62480        - Insertion_index:     100000
Selection_index:      90692        - Insertion_index:     100000
Done
UNIX> <font color=darkred><b></b></font>
</pre>

