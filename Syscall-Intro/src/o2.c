/* o2.c
   Jim Plank
   September, 1996 
   Latest revision: Thu Jan 27 08:38:48 EST 2011
*/

/* This program attempts to open the file "txt/out1.txt" for writing in the current
   directory.  Note that this fails because "txt/out1.txt" does not exist already.
   See src/o3.c for an example of opening "txt/out1.txt" properly.  */

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
  int fd;

  fd = open("txt/out1.txt", O_WRONLY);
  if (fd < 0) {
    perror("txt/out1.txt");
    exit(1);
  }
  return 0;
}
