<title>CS360 Lecture notes -- Introduction to System Calls (I/O System 
      Calls)</title>

<body bgcolor=ffffff>
<h1>CS360 Lecture notes -- Introduction to System Calls (I/O System Calls)</h1>
<UL>
<LI><a href=http://web.eecs.utk.edu/~jplank>James S. Plank</a>
<LI><a href=http://web.eecs.utk.edu/~jplank/plank/classes/cs360/>CS360</a>
<LI>Directory: <b>~jplank/cs360/notes/Syscall-Intro</b>
<LI>Lecture notes:
    <a href=http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Syscall-Intro/lecture.html>
    <b> http://web.eecs.utk.edu/~jplank/plank/classes/cs360/360/notes/Syscall-Intro/lecture.html </b></a>
<LI> September, 1996.
<LI> Last modification: Wed Feb  3 14:35:39 EST 2021
</UL>



<hr>
<h2>System Calls</h2>

When a computer is turned on, the program that gets executed first is
called the ``<i>operating system</i>.''  It controls pretty much all activity 
in the computer.  This includes who logs in, how disks are used, how
memory is used, how the CPU is used, and how you talk with other computers.
In this class, we focus on the Unix operating system, which is pervasive 
these days.
<p>

The way that programs talk to the operating system is via ``<i>system
calls</i>.'' A system call looks like a procedure call (see below),
but it's different -- <b>it is a request to the operating system to
perform some activity</b>.  

<p>
System calls are expensive.  While a procedure call can usually be 
performed in a few machine instructions, a system call requires the
computer to save its state, let the operating system take control 
of the CPU, have the operating system perform some function, have the
operating system save its state, and then have the operating system
give control of the CPU back to you.  This concept is important, and 
will be seen time and time again in this class.

<hr>
<h2>System Calls for I/O</h2>

There are 5 basic system calls that Unix provides for file I/O.
<pre>
        1.  int open(const char *path, int flags [ , int mode ] );
        2.  int close(int fd);
        3.  ssize_t read(int fd, void *buf, size_t count);
        4.  ssize_t write(int fd, const void *buf, size_t count);
        5.  off_t lseek(int fd, off_t offset, int whence);
</pre>

You'll note that they look like regular procedure calls.  This is how
you program with them -- like regular procedure calls.  However, you
should know that they are different: A system call makes a request to
the operating system.  A procedure call just jumps to a procedure
defined elsewhere in your program.  That procedure call may itself
make a system call (for example, <b>fopen()</b> calls <b>open()</b>), 
but it is a different call.  
<p>
The reason the operating system controls I/O is for safety -- the
computer must ensure that if my program has a bug in it, then it
doesn't crash the system, and it doesn't mess up other people's
programs that may be running at the same time or later.  Thus, 
whenever you do disk or screen or network I/O, you must go through
the operating system and use system calls.
<p>
These five system calls are defined fully in their man pages (do 
'<b>man -s 2 open</b>', '<b>man -s 2 close</b>', etc).  All those
irritating types like <b>ssize_t</b> and <b>off_t</b> are ints and
longs.  They used to all be ints, but as machines and files have
grown, so have they.

<hr>
<h2>Open</h2>

Open makes a request to the operating system to use a file.  The
'<b>path</b>' argument specifies what file you would like to use, and the
'<b>flags</b>' and '<b>mode</b>' arguments specify how you would like to use it.
If the operating system approves your request, it will return a
``<i>file descriptor</i>'' to you.  This is a non-negative integer.  If it
returns -1, then you have been denied access, and you have to check
the value of the variable "<b>errno</b>" to determine why.  (That or use
<b>perror()</b> -- see the 
<a href=../Chap1/lecture.html>basic terminology lecture notes</a>).
<p>
All actions that you will perform on files will be done through the
operating system.  Whenever you want to do file I/O directly with
the operating system, you specify the
file by its file descriptor.  Thus, whenever you want to do file I/O
on a specific file, you must first open that file to get a file
descriptor. 
<p>
Example: 
<b><a href=src/o1.c>src/o1.c</a></b>
opens the file 
<b><a href=txt/in1.txt>txt/in1.txt</a></b>
for reading, and prints the value
of the file descriptor.  If <b>txt/in1.txt</b doesn't exist, or you don't have permissions
to open it, then it will print -1, since the <b>open()</b> call fails.  If 
<b>txt/in1.txt</b> does exist,
then it will print 3, meaning that the <b>open()</b> request has been granted
(i.e. a non-negative integer was returned).

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program opens the file "txt/in1.txt" in the current directory, and prints out the
   return value of the open() system call.  If "txt/in1.txt" exists, open() will return a
   non-negative integer (three).  If "txt/in1.txt" does not exist, then it will return -1. */</font>

#include &lt;fcntl.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int main()
{
  int fd;

  fd = open("txt/in1.txt", O_RDONLY);
  printf("%d\n", fd);
  return 0;
}
</pre></td></table></center><p>

<p>
Note the value of '<b>flags</b>' -- the man page for 
<b>open()</b> will give you a description of the flags and how they work.
They are also described in 
<b><a href=file:/usr/include/fcntl.h>fcntl.h</a></b>, which can 
be found in the directory <tt> /usr/include</tt>. (Note that 
<b>fcntl.h</b> merely includes
<b><a href=file:/usr/include/sys/fcntl.h>
/usr/include/sys/fcntl.h</a></b>, so you'll have to look at that
file to see what <b>O_RDONLY</b> and all really mean).
<p>
Here are a few examples of calling <b>bin/o1</b>.  Initially, I have a file called <b>txt/in1.txt</b>
in my directory, so the <b>open()</b> call is successful, returning 3.  I then rename it to <b>tmp.ctxt</b>,
and now the <b>open()</b> call fails, return -1.  I rename it back, and the <b>open()</b> 
call succeeds again, returning 3:

<pre>
UNIX> <font color=darkred><b>ls -l txt/in1.txt</b></font>
-rw-r--r--  1 plank  staff  22 Jan 31 12:50 txt/in1.txt
UNIX> <font color=darkred><b>bin/o1 </b></font>
3                                      <font color=blue> # The open call succeeds here.</font>
UNIX> <font color=darkred><b>mv txt/in1.txt tmp.txt</b></font>
UNIX> <font color=darkred><b>bin/o1</b></font>
-1                                     <font color=blue> # The open call fails here.</font>
UNIX> <font color=darkred><b>mv tmp.txt txt/in1.txt</b></font>
UNIX> <font color=darkred><b>bin/o1</b></font>
3                                      <font color=blue> # The open call succeeds again.</font>
UNIX> <font color=darkred><b></b></font>
</pre>


Second example:
<b><a href=src/o2.c>src/o2.c</a></b>
tries to open the file "<b>txt/out1.txt</b>" for writing.  That fails
because <b>txt/out1.txt</b> does not exist already.  Here's the code --
you'll note that it uses <b>perror()</b> to print why the error occurred.


<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program attempts to open the file "txt/out1.txt" for writing in the current
   directory.  Note that this fails because "txt/out1.txt" does not exist already.
   See src/o3.c for an example of opening "txt/out1.txt" properly.  */</font>

#include &lt;fcntl.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;stdio.h&gt;

int main()
{
  int fd;

  fd = open("txt/out1.txt", O_WRONLY);
  if (fd &lt; 0) {
    perror("txt/out1.txt");
    exit(1);
  }
  return 0;
}
</pre></td></table></center><p>

We run it a few times below -- see the inline comments for commentary on what's happening.

<pre>
UNIX> <font color=darkred><b>ls -l txt</b></font>                                      <font color=blue> # As you can see, there's no txt/out1.txt</font>
total 8
-rw-r--r--  1 plank  staff  22 Jan 30  2018 in1.txt
-rw-r--r--  1 plank  staff   0 Jan 30  2018 out2.txt
UNIX> <font color=darkred><b>bin/o2</b></font>                                         <font color=blue> # Accordingly, then open() call fails.</font>
txt/out1.txt: No such file or directory
UNIX> <font color=darkred><b>echo Hi > txt/out1.txt</b></font>                         <font color=blue> # I create txt/out1.txt</font>
UNIX> <font color=darkred><b>bin/o2</b></font>                                         <font color=blue> # And now the open() call succeeds</font>
UNIX> <font color=darkred><b>cat txt/out1.txt</b></font>                               <font color=blue> # The program did not change the file.</font>
Hi
UNIX> <font color=darkred><b>chmod 0400 txt/out1.txt</b></font>                        <font color=blue> # Here I change the permissions so that I can't open for writing.</font>
UNIX> <font color=darkred><b>bin/o2</b></font>                                         <font color=blue> # And the open() call fails.</font>
txt/out1.txt: Permission denied
UNIX> <font color=darkred><b>chmod 0644 txt/out1.txt</b></font>
UNIX> <font color=darkred><b>rm txt/out1.txt</b></font>                                <font color=blue> # I remove the file</font>
UNIX> <font color=darkred><b>bin/o2</b></font>                                         <font color=blue> # And the open() call fails again.</font>
txt/out1.txt: No such file or directory
UNIX> <font color=darkred><b></b></font>
</pre>



In order to open a new file for
writing, you should open it with <b>(O_WRONLY | O_CREAT | O_TRUNC)</b> as
the <b>flags</b> argument.  The binary-or is how you aggregate these arguments
(they are each integers with a different bit set, so the binary-or combines them all).
<UL>
<LI> <b>O_CREAT</b> says to create the file if it doesn't exist already.
<LI> <b>O_TRUNC</b> say that if the file does exist, to "truncate" it to zero bytes, erasing
what was there.
</UL>
See 
<b><a href=src/o3.c>src/o3.c</a></b>
for an example:  

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program opens the file "out2.txt" for writing in the current directory.  It 
   uses O_CREAT to create the file if it does not exist already, and O_TRUNC to 
   truncate the file to zero bytes if it does exist. */</font>

#include &lt;fcntl.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int main()
{
  int fd;

  fd = open("txt/out2.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if (fd &lt; 0) {
    perror("txt/out2.txt");
    exit(1);
  }
  return 0;
}
</pre></td></table></center><p>

Below, I run <b>bin/o3</b> in various situations -- you can see that if the file doesn't
exist, it creates it.  If the file does exist, then it truncates it:

<pre>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # txt/out2.txt has zero bytes and was last changed in 2018</font>
-rw-r--r--  1 plank  staff  0 Jan 30  2018 txt/out2.txt
UNIX> <font color=darkred><b>bin/o3</b></font>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # It still has zero bytes, but the modification time has updated.</font>
-rw-r--r--  1 plank  staff  0 Feb  3 14:56 txt/out2.txt
UNIX> <font color=darkred><b>rm txt/out2.txt</b></font>
UNIX> <font color=darkred><b>bin/o3</b></font>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # Now it created the file anew.</font>
-rw-r--r--  1 plank  staff  0 Feb  3 14:57 txt/out2.txt
UNIX> <font color=darkred><b>echo "Hi" > txt/out2.txt</b></font>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # The echo command has put "Hi" and a newline into the file.</font>
-rw-r--r--  1 plank  staff  3 Feb  3 14:57 txt/out2.txt
UNIX> <font color=darkred><b>bin/o3</b></font>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # bin/o3 has truncated the file.</font>
-rw-r--r--  1 plank  staff  0 Feb  3 14:57 txt/out2.txt
UNIX> <font color=darkred><b>echo "Hi Again" > txt/out2.txt</b></font>
UNIX> <font color=darkred><b>chmod 0400 txt/out2.txt</b></font>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # I have put 9 bytes into the file using echo, but the permission is read-only.</font>
-r--------  1 plank  staff  9 Feb  3 14:57 txt/out2.txt
UNIX> <font color=darkred><b>bin/o3</b></font>                                                <font color=blue> # As such, bin/o3 fails to open the file.</font>
txt/out2.txt: Permission denied
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # And the file is unchanged.</font>
-r--------  1 plank  staff  9 Feb  3 14:57 txt/out2.txt
UNIX> <font color=darkred><b>chmod 0644 txt/out2.txt</b></font>
UNIX> <font color=darkred><b>bin/o3</b></font>
UNIX> <font color=darkred><b>ls -l txt/out2.txt</b></font>                                    <font color=blue> # When I change the permissions back to R/W, bin/o3 truncates the file again.</font>
-rw-r--r--  1 plank  staff  0 Feb  3 14:58 txt/out2.txt
UNIX> <font color=darkred><b></b></font>
</pre>



Finally, the '<b>mode</b>' argument should only be used if you are creating
a new file.  It specifies the protection mode of the new file.  <tt>0644</tt>
is the most typical value -- it says "I can read and write it;
everyone else can only read it."  The zero in <tt>0644</tt> says to interpret
the number in octal.  (Later, when you learn about the <b>umask</b>, you'll
use a different <b>mode</b>, but for now, we'll use <tt>0644</tt>).
<p>
You can open the same file more than once.  You will get a different
<b>fd</b> each time.  If you open the same file for writing more than once
at a time, you may get bizarre results. 
<hr>
<h2>Close</h2>

<b>Close()</b> tells the operating system that you are done with a 
file descriptor.
The OS can then reuse that file descriptor.  The program
<b><a href=src/c1.c>src/c1.c</a></b>
shows some 
examples with opening and closing the file <b>txt/in1.txt</b>.  You should look at
it carefully, as it opens the file multiple times without closing it, which is
perfectly legal in Unix.

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program opens and closes the file "txt/in1.txt" in a variety of ways.
   Make sure you understand this program, especially the return values of the open calls. */</font>

#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;fcntl.h&gt;

int main()
{
  int fd1, fd2;

  <font color=blue>/* First open txt/in1.txt twice for reading.  Print out the file descriptors. */</font>

  fd1 = open("txt/in1.txt", O_RDONLY);
  if (fd1 &lt; 0) { perror("c1"); exit(1); }

  fd2 = open("txt/in1.txt", O_RDONLY);
  if (fd2 &lt; 0) { perror("c1"); exit(1); }

  printf("Opened the file txt/in1.txt twice:  Fd's are %d and %d.\n", fd1, fd2);

  <font color=blue>/* Close the file descriptors. */</font>

  if (close(fd1) &lt; 0) { perror("c1"); exit(1); }
  if (close(fd2) &lt; 0) { perror("c1"); exit(1); }

  printf("Closed both fd's.\n");

  <font color=blue>/* Open txt/in1.txt again, to see that it will reuse the first file descriptor. */</font>

  fd2 = open("txt/in1.txt", O_RDONLY);
  if (fd2 &lt; 0) { perror("c1"); exit(1); }
  
  printf("Reopened txt/in1.txt into fd2: %d.\n", fd2);

  <font color=blue>/* Close the file descriptor twice.  The second causes an error, which usually
     goes unnoticed, because programmers rarely look at the return value of close(). */</font>

  if (close(fd2) &lt; 0) { perror("c1"); exit(1); }

  printf("Closed fd2.  Now, calling close(fd2) again.\n");
  printf("This should cause an error.\n\n");

  if (close(fd2) &lt; 0) { perror("c1"); exit(1); }
  return 0;
}
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>bin/c1</b></font>
Opened the file txt/in1.txt twice:  Fd's are 3 and 4.
Closed both fd's.
Reopened txt/in1.txt into fd2: 3.
Closed fd2.  Now, calling close(fd2) again.
This should cause an error.

c1: Bad file descriptor
UNIX> <font color=darkred><b></b></font>
</pre>


<hr>
<h2>Read</h2>

<b>Read()</b> tells the operating system to read "<b>size</b>" 
bytes from the file opened
in file descriptor "<b>fd</b>", and to put those bytes into the location pointed
to by "<b>buf</b>".  It returns how many bytes were actually read.  Consider
the code in
<b><a href=src/r1.c>src/r1.c</a></b>

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program shows some simple examples of using the system call read() to read from a file. */</font>

#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;fcntl.h&gt;
#include &lt;unistd.h&gt;

int main()
{
  char *c;
  int fd, sz;

  <font color=blue>/* Allocate 100 bytes, and then open txt/in1.txt: */</font>

  c = (char *)</font> malloc(100 * sizeof(char));
  fd = open("txt/in1.txt", O_RDONLY);
  if (fd &lt; 0) { perror("r1"); exit(1); }

  <font color=blue>/* Read ten bytes from the file.  Print the return value, 
     add the NULL character, and print the bytes as a string. */</font>

  sz = read(fd, c, 10);
  printf("called read(%d, c, 10).  returned that %d bytes  were read.\n", fd, sz);
  c[sz] = '\0';
  printf("Those bytes are as follows: %s\n", c);

  <font color=blue>/* Now, read 99 bytes and do the same thing.  You'll note that since there 
     were only 12 more bytes in the file, that read() returns 12.  Also, you'll
     note that read() does not NULL terminate anything.  It simply reads the
     bytes.  So you need to NULL terminate before printing. */</font>

  sz = read(fd, c, 99);
  printf("called read(%d, c, 99).  returned that %d bytes  were read.\n", fd, sz);
  c[sz] = '\0';
  printf("Those bytes are as follows: %s\n", c);

  <font color=blue>/* As with freeing memory, this is unnecessary, since we are exiting.
     The operating system will make sure that open files are closed when
     the process exits. */</font>

  close(fd);
  return 1;
}
</pre></td></table></center><p>


When executed, you get the following:
<pre>
UNIX> <font color=darkred><b>bin/r1</b></font>
called read(3, c, 10).  returned that 10 bytes  were read.
Those bytes are as follows: Jim Plank

called read(3, c, 99).  returned that 12 bytes  were read.
Those bytes are as follows: Claxton 221

UNIX> <font color=darkred><b></b></font>
</pre>

There are a few things to note about this program.  First, <b>buf</b>
should point to valid memory.  In <b>src/r1.c</b>, this is achieved by
<b>malloc()</b>-ing space for <b>c</b> Alternatively, I could have
declared <b>c</b> to be a static array with 100 characters:
<pre>
  char c[100];
</pre>
Second, I null terminate <b>c</b> after the <b>read()</b> calls to ensure
that <b>printf()</b> will understand it.  This is important -- in text files,
there are no NULL characters.  When <b>read()</b> reads them, it does not
NULL terminate.  If you are going to use the characters as strings in C, you'll
need to NULL terminate them yourself.
<p>
Third, when  <b>read()</b> returns 0, then the end of file has been reached.
When you are reading from a file, if <b>read()</b> returns fewer bytes
than you requested, then you have reached the end of the file as well.  
This is what happens in the second <b>read()</b> in <b>src/r1.c</b>.
<p>
Fourth, note that the 10th character in the first <b>read()</b> call 
and the 12th character in the second are both
newline characters.  That is why you get two newlines in the <b>printf()</b>
statement.  One is in <b>c</b>, and the other is in the <b>printf()</b>
statement.
<p>
To reiterate, the read call does <i>not</i> read a NULL character.  It simply reads bytes from the
file, and the file does not contain any NULL characters.   This is why you have to put the
NULL character explicitly into your string.  Let's take a look at a similar program, which
doesn't NULL terminate (<b><a href=src/r2.c>src/r2.c</a></b>):

<b><a href=src/r2.c>src/r2.c</a></b>

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Showing what happens when you don't NULL terminate. */</font>

#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
#include &lt;fcntl.h&gt;
#include &lt;unistd.h&gt;
#include &lt;string.h&gt;

int main()
{
  char c[100];
  int fd;

  strcpy(c, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  fd = open("txt/in1.txt", O_RDONLY);
  if (fd &lt; 0) { perror("r1"); exit(1); }

  read(fd, c, 10);            <font color=blue>/* I read 10 bytes, but I don't null terminate. */</font>
  printf("%s\n", c);          <font color=blue>/* So this printf() will print the characters from K to Z. */</font>

  read(fd, c, 99);            <font color=blue>/* This reads 12 bytes, so it prints M to Z. */</font>
  printf("%s\n", c);

  return 0;
}
</pre></td></table></center><p>
Because I didn't NULL terminate after reading, <b>printf()</b> prints every character
of <tt>c</tt> until it encounters the NULL character after <tt>'Z'</tt>.  That's why you
get the stray uppercase letters at the end of each <b>printf()</b> statement:

<pre>
UNIX> <font color=darkred><b>bin/r2</b></font>
Jim Plank
KLMNOPQRSTUVWXYZ
Claxton 221
MNOPQRSTUVWXYZ
UNIX>
</pre>


<hr>
<h2>Write</h2>

<b>Write()</b> is just like <b>read()</b>, only it writes the bytes 
instead of reading them.
It returns the number of bytes actually written, which is almost 
invariably "<b>size</b>".
<p>
<b><a href=src/w1.c>src/w1.c</a></b>
writes the string <b>"cs360\n"</b> to the file <b>out3.txt</b>.

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program opens the file "out3.txt" in the current directory 
   for writing, and writes the string "cs360\n" to it. */</font>

#include &lt;fcntl.h&gt;
#include &lt;stdio.h&gt;
#include &lt;unistd.h&gt;
#include &lt;string.h&gt;
#include &lt;stdlib.h&gt;

int main()
{
  int fd, sz;

  fd = open("txt/out3.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if (fd &lt; 0) { perror("txt/out3.txt"); exit(1); }

  sz = write(fd, "cs360\n", strlen("cs360\n"));

  printf("called write(%d, \"cs360\\n\", %ld).  it returned %d\n",
         fd, strlen("cs360\n"), sz);

  close(fd);
  return 0;
}
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>bin/w1</b></font>
called write(3, "cs360\n", 6).  it returned 6
UNIX> <font color=darkred><b>cat txt/out3.txt</b></font>
cs360
UNIX> <font color=darkred><b></b></font>
</pre>


You should think about different combinations of <b>O_CREAT</b> and <b>O_TRUNC</b>, and their
effect on <b>write</b>.  In particular, take a look at 
<b><a href=src/w2.c>src/w2.c</a></b>.  This lets you specify the combination of <b>O_WRONLY</b>,
<b>O_CREAT</b> and <b>O_TRUNC</b> that you use in your <b>open()</b> call:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program opens the file "txt/out3.txt" in the current 
   directory for writing, allows you to specify the combination
   of O_CREAT and O_TRUNC, plus what you write to the file.  */</font>

#include &lt;fcntl.h&gt;
#include &lt;stdio.h&gt;
#include &lt;unistd.h&gt;
#include &lt;string.h&gt;
#include &lt;stdlib.h&gt;

int main(int argc, char **argv)
{
  int fd, sz, flags, len;

  if (argc != 3) {
    fprintf(stderr, "usage: w2 w|wc|wt|wct input-word\n");
    exit(1);
  }

  <font color=blue>/* Figure out what the "flags" argument will be to the open() call. */</font>

  if (strcmp(argv[1], "w") == 0) {
    flags = O_WRONLY;
  } else if (strcmp(argv[1], "wc") == 0) {
    flags = (O_WRONLY | O_CREAT);
  } else if (strcmp(argv[1], "wt") == 0) {
    flags = (O_WRONLY | O_TRUNC);
  } else if (strcmp(argv[1], "wct") == 0) {
    flags = (O_WRONLY | O_CREAT | O_TRUNC);
  } else {
    fprintf(stderr, "Bad first argument.  Must be one of w, wc, wt, wct.\n");
    exit(1);
  }

  <font color=blue>/* Open the file with the given flags. */</font>

  fd = open("txt/out3.txt", flags, 0644);
  if (fd &lt; 0) { perror("open"); exit(1); }

  len = strlen(argv[2]);
  sz = write(fd, argv[2], len);
  
  <font color=blue>/* Write the input word to the file. */</font>

  printf("called write(%d, \"%s\", %d).  It returned %d\n", fd, argv[2], len, sz);

  close(fd);
  return 0;
}
</pre></td></table></center><p>

Take a look at all of the following executions of the program.  You should be able 
to explain them all.  You should also notice that there is no newline in the write
call, which is why the resulting file has no newline in it.  There is also no NULL
character being written to the file, because you are writing <b>strlen()</b> bytes,
which does not include the NULL character:

<pre>
UNIX> <font color=darkred><b>bin/w2</b></font>
usage: w2 w|wc|wt|wct input-word
UNIX> <font color=darkred><b>rm -f txt/out3.txt</b></font>                                      <font color=blue> # Make sure there's no txt/out3.txt</font>
UNIX> <font color=darkred><b>ls -l txt/out*</b></font>
-rw-r--r--  1 plank  staff  0 Feb  3 14:58 txt/out2.txt
UNIX> <font color=darkred><b>bin/w2 w Hi</b></font>                                             <font color=blue> # The open() fails because the file doesn't exist, and we didn't specify O_CREAT</font>
txt/out3.txt: No such file or directory
UNIX> <font color=darkred><b>ls txt/out*</b></font>
txt/out2.txt
UNIX> <font color=darkred><b>bin/w2 wc ABCDEFG</b></font>                                       <font color=blue> # Because of O_CREAT, the file is created.</font>
called write(3, "ABCDEFG", 7).  It returned 7
UNIX> <font color=darkred><b>ls -l txt/out*.txt</b></font>
-rw-r--r--  1 plank  staff  0 Feb  3 14:58 txt/out2.txt
-rw-r--r--  1 plank  staff  7 Feb  4 17:14 txt/out3.txt       <font color=blue> # It's 7 bytes because of the write().</font>
UNIX> <font color=darkred><b>cat txt/out3.txt</b></font>
ABCDEFGUNIX> <font color=darkred><b></b></font>                                                 <font color=blue> # We didn't write a newline, so it doesn't print one.</font>
UNIX> <font color=darkred><b>bin/w2 w XYZ</b></font>                                            <font color=blue> # I type ENTER to get the prompt looking nice,</font>
called write(3, "XYZ", 3).  It returned 3                     <font color=blue> # and I write three bytes</font>
UNIX> <font color=darkred><b>ls -l txt/out3.txt</b></font>
-rw-r--r--  1 plank  staff  7 Feb  4 17:14 txt/out3.txt       <font color=blue> # The file is still 7 bytes, because I didn't call with O_TRUNC</font>
UNIX> <font color=darkred><b>cat txt/out3.txt</b></font>
XYZDEFGUNIX> <font color=darkred><b></b></font>                                                 <font color=blue> # It overwrote the "ABC" with "XYZ".</font>
UNIX> <font color=darkred><b>bin/w2 wc ---</b></font>                                           <font color=blue> # O_CREAT is specified, but the file exists, so it does nothign.  I didn't truncate.</font>
called write(3, "---", 3).  It returned 3
UNIX> <font color=darkred><b>ls -l txt/out3.txt</b></font>
-rw-r--r--  1 plank  staff  7 Feb  4 17:15 txt/out3.txt       <font color=blue> # Still 7 bytes.</font>
UNIX> <font color=darkred><b>cat txt/out3.txt</b></font>
---DEFGUNIX> <font color=darkred><b></b></font>                                                 <font color=blue> # And the "XYZ" is replaced with "---".</font>
UNIX> <font color=darkred><b>bin/w2 wt abcde</b></font>                                         <font color=blue> # Now, I specify O_TRUNC</font>
called write(3, "abcde", 5).  It returned 5
UNIX> <font color=darkred><b>ls -l txt/out3.txt</b></font>
-rw-r--r--  1 plank  staff  5 Feb  4 17:16 txt/out3.txt       <font color=blue> # And the file is 5 bytes now, rather than 7</font>
UNIX> <font color=darkred><b>cat txt/out3.txt</b></font>
abcdeUNIX> <font color=darkred><b></b></font>                                                   <font color=blue> # Still no newline.</font>
UNIX> <font color=darkred><b>rm txt/out3.txt</b></font>
UNIX> <font color=darkred><b>bin/w2 wt fghij</b></font>                                         <font color=blue> # This fails because the file doesn't exist, and I didn't specify O_CREAT.</font>
txt/out3.txt: No such file or directory
UNIX> <font color=darkred><b></b></font>
</pre>


<hr>
<h2>Lseek</h2>

All open files have a "<i>file pointer</i>" associated with them.  When the file
is opened, the file pointer points to the beginning of the file.  As the
file is read or written, the file pointer moves.  For example, in <b>r1.c</b>,
after the first read, the file pointer points to the 11th byte in <b>txt/in1.txt</b>.
You can move the file pointer manually with <b>lseek()</b>.  The 'whence' 
variable
of <b>lseek</b> specifies how the seek is to be done -- from the beginning of the
file, from the current value of the pointer, and from the end of the file.
The return value is the offset of the pointer after the lseek.  Look 
at <b><a href=src/l1.c>src/l1.c</a></b>
It does a bunch of seeks on the file <b>txt/in1.txt</b>.  Trace it and make
sure it all makes sense.  How did I know to include <b>sys/types.h</b> and 
<b>unistd.h</b>?  I typed "<b>man -s 2 lseek</b>".

<hr>
<h2>Standard Input, Standard Output, and Standard Error</h2>

Now, every process in Unix starts out with three file descriptors predefined and open:
<ul>
<li>        File descriptor 0 is standard input.
<li>        File descriptor 1 is standard output.
<li>        File descriptor 2 is standard error.
</ul>
Thus, when you write a program, you can read from standard input, using
<b>read(0, ...)</b>, and write to standard output using <b>write(1, ...)</b>.
<p>
Armed with that information, we can write a very simple <b>cat</b> program (one that copies standard
input to standard output) with one line: (this is in 
<b><a href=src/simpcat.c>src/simpcat.c</a></b>):

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;unistd.h&gt;

int main()
{
  char c;

  while (read(0, &c, 1) == 1) write(1, &c, 1);
  return 0;
}
</pre></td></table></center><p>

You'll note, because I am only calling the system calls <b>read()</b> and <b>write()</b>,
I don't need to include <tt>stdio.h</tt> or <tt>stdlib.h</tt>.

<pre>
UNIX> <font color=darkred><b>bin/simpcat < txt/in1.txt</b></font>
Jim Plank
Claxton 221
UNIX> <font color=darkred><b></b></font>
</pre>
