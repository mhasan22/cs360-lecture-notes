#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  int t;

  t = (fork() == 0) ? atoi(argv[1]) : atoi(argv[2]) ;
  sleep(t);
  return 0;
}
