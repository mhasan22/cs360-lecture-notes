#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  int i, j, k, l;
  int *ip;

  i = 0xaaaaaaaa;
  j = 0xbbbbbbbb;
  k = 0xcccccccc;
  l = 0xdddddddd;

  ip = &k;
  printf("&i: 0x%x\n", (unsigned int) &i);
  printf("&j: 0x%x\n", (unsigned int) &j);
  printf("&k: 0x%x\n", (unsigned int) &k);
  printf("&l: 0x%x\n", (unsigned int) &l);
  printf("ip: 0x%x\n", (unsigned int) ip);

  strcpy((char *) ip, "abcdefg");
  printf("0x%x\n", i);
  printf("0x%x\n", j);
  printf("0x%x\n", k);
  printf("0x%x\n", l);
  
  return 0;
}

