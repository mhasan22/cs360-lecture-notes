/* fs3a.c
   Jim Plank
   CS360
   Dup lecture 

   This shows how an open stdio buffer is duplicated over the fork call.
   There are no suprises here. */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main()
{
  char s[1000];
  int i;
  FILE *f;

  f = fopen("file3", "w");

  i = fork();
  fprintf(f, "fork() = %d.\n", i);
  fclose (f);
  return 0;
}

