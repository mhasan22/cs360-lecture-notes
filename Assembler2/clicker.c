#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  int *i;

  void *a;
  size_t len;

  len = 8192;

  srand(50);

  a = mmap((void *) 0x654000, len, PROT_READ | PROT_WRITE, MAP_ANON | MAP_FIXED | MAP_PRIVATE, -1, 0);
  if (a == MAP_FAILED) {
    perror("mmap");
    exit(1);
  }

  i = (int *) 0x654320;
  *i = 0x765430;

  printf("i's value:   0x%lx\n", (unsigned long) i);
  printf("i's address: 0x%lx\n", (unsigned long) &i);

  printf("0x%lx\n", (unsigned long) &(i[1]));
  return 0;
}
