#include <stdio.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
  int status;

  printf("Hi\n");
  if (fork() == 0) {
    printf("I am a child.\n");
  } else {
    printf("I am no child.\n");
    wait(&status);
  }
  return 0;
}
