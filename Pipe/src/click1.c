#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
  if (fork() == 0) printf("Fred %d\n", getpid());
  if (fork() == 0) printf("Fred %d\n", getpid());
  return 0;
}
