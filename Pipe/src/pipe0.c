/* pipe0.c - Create a pipe in the current process, write to it and read from it. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
  int pipefd[2];
  int i;
  char s[1000];
  char *s2;

  /* Create the pipe. */
  if (pipe(pipefd) < 0) {
    perror("pipe");
    exit(1);
  }

  /* Write an 11-byte string to it.  This gets stored in the operating system. */

  s2 = "James Plank";
  write(pipefd[1], s2, strlen(s2));

  /* Now read the string from the pipe.  Even though we ask for 1000 bytes,
     it simply returns the 11 bytes that are in the pipe. */ 

  i = read(pipefd[0], s, 1000);
  s[i] = '\0';

  printf("Read %d bytes from the pipe: '%s'\n", i, s);
  return 0;
}
