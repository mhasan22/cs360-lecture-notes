#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
  int status;

  printf("Hi\n");
  if (fork() == 0) {
    execlp("cat", "cat", "fred.txt", NULL);
    execlp("cat", "cat", "fred.txt", NULL);
    execlp("cat", "cat", "fred.txt", NULL);
    exit(1);
  } else {
    (void) wait(&status);
    printf("Done\n");
  }
  return 0;
}
