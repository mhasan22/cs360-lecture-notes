#include <stdio.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
  int status;

  printf("Hi\n");
  if (fork() == 0) {
    execlp("cat", "cat", "fred.txt", NULL);
  } else {
    wait(&status);
    printf("Done\n");
  }
  return 0;
}
